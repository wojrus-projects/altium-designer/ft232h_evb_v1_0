# Specyfikacja

Urządzenie jest uniwersalną płytką uruchomieniową do FT232HL (https://ftdichip.com/products/ft232hl).

# Projekt PCB

Schemat: [doc/FT232H_EVB_V1_0_SCH.pdf](doc/FT232H_EVB_V1_0_SCH.pdf)

Widok 3D: [doc/FT232H_EVB_V1_0_3D.pdf](doc/FT232H_EVB_V1_0_3D.pdf) (wymaga *Adobe Acrobat Reader DC*)

Rysunek montażowy: [doc/FT232H_EVB_V1_0_ASSEMBLY_TOP.pdf](doc/FT232H_EVB_V1_0_ASSEMBLY_TOP.pdf)

# Licencja

MIT
